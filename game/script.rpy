﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define charPlayer = Character("You")
define charStranger = Character("Stranger", color="#ffd000")
define charBusDriver = Character("Bus Driver", color="#002fff")

## Non Human Character
define charRadioNews = Character("Radio")
define charUnknown = Character("???")
define charEMail = Character("E-Mail")

## Debug Characters
define debugProgrammer = Character("The Programmer", color="#ff0000", what_color="#ff0000")
define debugError = Character("Error", color="#ff0000", what_color="#ff0000")

# variables

## encounter flags
define flag_encounteredStrangerSteppingOnBranches = False

## collected items flags
define armorShield = 0

# layers
init python:
    renpy.add_layer("ambientOverlay", "master", None, False)

# screens
## ending screens
screen endingScreen(endingText):
    style_prefix "centered"
    text endingText

style centered_text:
    text_align 0.5
    layout "subtitle"

## images buttons
### Example
screen redButton():
    imagebutton:
        xalign 0.5
        yalign 0.5
        auto "imagebutton %s.png"
        action [ToggleScreen("redButton"), Jump(label_name)]

        ### call screen redButton

screen buttonsBedroomMaster():
    imagebutton:
        xalign 0.98
        yalign 0.62
        auto "circle interact %s.png"
        sensitive 
        #action Function(global.skullFound)
        action [SetVariable("skull2", 1), SetVariable("skull3", 1)]
    imagebutton:
        xalign 0.15
        yalign 0.9
        auto "arrow front 4D %s.png"
        action [ToggleScreen("buttonsBedroomMaster"), Jump("mansionBathroomMaster"), SetVariable("lastRoom","mansionBedroomMaster")]
    imagebutton:
        xalign 0.95
        yalign 0.95
        auto "arrow back 4D %s.png"
        action [ToggleScreen("buttonsBedroomMaster"), Jump(lastRoom)]


# music

define audio.bgm_main = "music/Main_Theme_Bus_Boi.mp3"
define audio.bgm_start = "music/I_see_him.mp3"
define audio.amb_thunderStorm = "audio/amb_Thunderstorm_Loop_158s.mp3"
define audio.amb_wind = "audio/amb_wind_general_soft_low_loop_05.wav"

# positions
define rightCenter = Position(xpos=0.4)

# transitions
## Camera flash - quickly fades to white, then back to the scene.
## Fade(out_time, hold_time, in_time, *, color='#000')
define flash = Fade(0.1, 0.1, 0.5, color="#fff")

# Declare other Variables
## Declare the new Image Format "Movie"
image movie = Movie(size=(1920,1080), xpos=0, ypos=0, xanchor=0, yanchor=0)

init -10 python:
    def flipImage(s):
        return Transform(s, xzoom = -1.0)

    config.displayable_prefix["flip"] = flipImage

# animations
init:
    image rain:
        "fg_rainDrops_001.png"
        0.1
        "fg_rainDrops_002.png"
        0.1
        "fg_rainDrops_003.png"
        0.1
        "fg_rainDrops_004.png"
        0.1
        "fg_rainDrops_005.png"
        0.1
        "fg_rainDrops_006.png"
        0.1
        repeat

######################################################################################################
######################################## The game starts here ########################################
######################################################################################################

label start:
    scene black
    show fg vignette001 onlayer ambientOverlay
    show rain onlayer ambientOverlay
    stop music fadeout 1.0
    pause 1.0

    play music bgm_start fadein 1.0
    play ambient amb_thunderStorm fadein 1.0 volume 0.6

    charPlayer "No...please no!"
    charPlayer "Why?{p}How can you betray me like that?"
    charPlayer "Only 4 percent battery capacity left?! But I fully recharged you just this morning. Damn cell phone!" 
    charPlayer "First I have to walk for miles to hopefully arrive at this godforsaken bus stop at some point. {w}Then of course it starts raining cats and dogs, just as I'm about to leave."
    charPlayer "And now this!"
    charPlayer "I don't even have internet in this wasteland.{p}Today must really be... {w}my absolute lucky day."

    narrator "You just hope that you will actually reach this bus stop any time soon. But honestly, who builds a bus stop here in Neverland anyway? In the damn woods?!"
    narrator "Who even does the bus pick up here? {w}Bigfoot? {w}Trolls? {w}Bambi and Thumper? {w}There are literally just trees around here."
    narrator "But yes, you should have left earlier. You could have been sitting at home right now!"
    narrator "Instead you are wandering around in pitch black darkness. Drenched from the heavy rain. With your phone about to die."

    narrator "What was that? Like someone stepping on branches."
    narrator "Pretty loud."
    narrator "Pretty close."

    menu checkOutTheNoise:
        "Stop and turn around to see what made this noise.":
            jump checkingOutTheNoise
        "Ignore it and move on.":
            jump ignoringTheNoise



label checkingOutTheNoise:
    narrator "When you turn around to see what or who could have caused that noise, you are sure you saw something disappearing into the forest."
    narrator "Somethin big and heavy. Waaay too big for an ordinary deer. Or so you think at least."
    $ flag_encounteredStrangerSteppingOnBranches = True
    narrator "Thanks to the cell phone's low battery you can't even activate the damn flashlight. The only thing you see there in the darkness while rain is blowing in your face... {w}are trees."
    narrator "At moments like this you really wish you had spent more time outdoors."
    narrator "Without any chance of shedding more light into the darkness, you have no choice but to move on. And hoping that whatever it was doesn't come back."

    jump movingOnToTheBusStop



label ignoringTheNoise:

    narrator "Must have been the wind. Probably just some animals that are more afraid of you than you are of them. Hopefully anyway."
    narrator "So you just keep walking and hope that the bus stop is soon in sight. The sooner the better."

    jump movingOnToTheBusStop



label movingOnToTheBusStop:

    narrator "Your hope that these huge trees around you would at least stop the rain a little has not come true."
    narrator "And with your phone's flashlight deactivated, the only thing that helps you find the way is the faint light of the moon shining through the few open spaces between the trees."
    narrator "You look at your phone and see that it's almost midnight. About 15 minutes until the witching hour."
    narrator "Witching hour, huh? Given your current luck, there will probably be a ghost bus waiting for you there."

    if flag_encounteredStrangerSteppingOnBranches:
        narrator "Wait!"
        narrator "There was it again! That noise. You look around. But... nothing. Nothing but trees and rain. No moving shadow this time."
        narrator "Did you really hear something or was it just your imagination playing tricks on you?"

    narrator "But you should probably pick up the pace anyway since the bus arrives at midnight. Where is the fast travel option when you need it?"
    narrator "And just then - there in the distance - a slight glow of light. That must be the bus stop. It just must be."
    narrator "As you get closer, you're sure about it. Finally! The bus stop. As if the stop took pity on you... well, or as if it was just tired of your whining."

    show bg bus stop with fade

    narrator "Whatever it was, you are happy to get at least some sort of protection from the storm."
    narrator "And hey! You probably even reached your goal of walking 10.000 steps today. First time in months."
    narrator "Okay, first time ever. You once saw it in a video where some dude said walking 10.000 steps a day keeps you healthy."
    narrator "Super healthy and protected from the rain? Maybe it really is going to be your lucky day."
    narrator "The bus stop appears like a safe haven surrounded by an ocean of darkness. The inviting warmth of the shelter's light against the dark sharp shadows of the trees towering over the stop."
    narrator "It almost seems as if it is trying to ward off the dancing shadows around it with its bright light emanating from the lamps on its ceiling."
    narrator "Only the old wooden bench looks quite uncomfortable. But oh well, you take what you get."

    narrator "As you step into the light of the bus stop shelter, you feel your tension melt away."
    narrator "Finally you can sit down. You exhale deeply. You're basically already home. And if you weren't completely soaked from the rain, this would probably be almost cozy."
    charPlayer "Ugh. What's this smell?"
    narrator "You spot a garbage can on the side of the shelter."
    charPlayer "I hope the bus is more reliable than the garbage collection here."
    narrator "Well at least the storm lets the wet garbage stench waft over only every now and then."
    narrator "You just have to get through 10 more minutes. Then you're finally on your way home. Out of the rain. Away from this deserted place."

    narrator "There in the distance. From the direction you came from. You seem to be able to make out a shadowy figure."
    narrator "Now blinded by the light of the stop, you have to squint to see anything out there."
    narrator "But yes! There really is something out there. Something big and it's heading straight towards the bus stop... {w}towards you."
    narrator "At this time? In this no-man's-land? Maybe... maybe it's just another unlucky guy trying to catch the bus."
    narrator "But there's something strange about this guy. Maybe it's just your nerves because you're completely alone in the forest during a thunderstorm at midnight, waiting for a bus."

    menu reactToTheStrangerApproachingBusStop:
        "Should you leave or should you stay?"
        "Stay.":
            jump stayingAtTheBusStop
        "Leave.":
            jump leavingTheBusStop
    
    jump ending_failSave
        


label leavingTheBusStop:

    narrator "Yes, you think it's better to go for a walk and come back when it's midnight."
    narrator "If the guy is still there, you can just get on the bus straight away without having to deal with him. That seems to be the safest option."

    scene black with fade
    
    narrator "But as you get further and further away from the light of the bus stop, you hear footsteps behind you."
    narrator "The footsteps are getting faster."
    narrator "... closer."

    menu runFromTheStranger:
        "RUN!":
            narrator "As you start running, you hear the footsteps behind you getting faster as well."
            narrator "Faster and faster. Coming closer and closer"
            narrator "You rush forward, further and further into the dark nothingness."
            narrator "What does he want?"
            narrator "Why is he running after you?"
            narrator "You gasp for air as you try to ignore the stitches in your side."
            narrator "You just wanted to catch the bus home!"
            narrator "All you can think about is run. Just get away from this lunatic."
            narrator "RUN!"
            narrator "After you've run for what feels like forever, you're at the end of your tether and completely out of breath. That's when you hear"
            narrator "... nothing."
            narrator "You stop. You take a few deep breaths. You wipe the rain from your face and look back."
            narrator "Nothing there. No footsteps. No lunatic."
            narrator "But you're sure there was something there. Something."
            narrator "Suddenly! THAT NOISE!"
            narrator "Something tackles you from the side and pushes you to the ground."
            narrator "You can't move anymore. This thing is on top of you and it's huge and heavy."
            narrator "It almost looks like a bear. Pushing you to the ground."
            charPlayer "WHAT DO YOU WANT FROM ME?! Let me go!"
            narrator "Almost instantly, the creature stops moving. As if it were frozen in time."
            charPlayer "What tHE F-"
            narrator "Last thing you see, are two glowing white eyes looking at you - almost smiling."
            
            stop music fadeout 1.0
            stop ambient fadeout 1.0
            hide rain onlayer ambientOverlay
            scene black with fade
            show screen endingScreen("Bad Ending 1/3\n\nYou never caught the bus home,\nbut something caught you.")
            pause
            
            # This ends the game.
            return

        "Take a look at what is following you.":
            narrator "You take a deep breath and turn around."
            if flag_encounteredStrangerSteppingOnBranches:
                narrator "You know that silhouette. This is the creature you saw disappearing between the trees. The ominous shadow that had made the loud noise."
            
            narrator "That's definitely no deer. But whatever it is, it's coming towards you fast!"
            narrator "As you take a few steps back, you trip over something."
            narrator "Before you can even look at the thing you tripped over, you see that the creature is just only a few meters away from you. Still running towards you."
            narrator "You try to get up when the huge creature pounces on you."
            
            stop music fadeout 1.0
            stop ambient fadeout 1.0
            hide rain onlayer ambientOverlay
            scene black with fade
            show screen endingScreen("Bad Ending 2/3\n\nThe bus arrived at the stop on time,\nbut you were never seen again.")
            pause
            
            # This ends the game.
            return

    jump ending_failSave



label stayingAtTheBusStop:

    narrator "The strange figure comes closer and closer."
    narrator "You try to distract yourself from the stranger with your cell phone. Only 3 percent. Great. But what the heck, there is no mobile network here anyway."
    narrator "You glance over at the shadowy figure. You can make out a rough silhouette. Quite big and chunky. It strolls somewhat ponderously towards the stop."
    narrator "Looking at the stranger, you're not even sure if the two of you will fit on the bench together."
    narrator "Just as you are about to check your phone again, you hear a terribly friendly voice."
    
    charStranger "Sooooo... {w}I'm not the only one here waiting late at night. How nice."

    menu arriveAtBusStop:
        # "What do you do?"
        "Talk to the Stranger.":
            jump talkingToTheStranger
        "Try to look busy.":
            jump tryingToLookBusy
        "Just ignore him.":
            jump ignoringTheStranger

    jump ending_failSave



label talkingToTheStranger:

    narrator "Might as well talk to that strange guy. Maybe it will help to kill some time."
    
    charPlayer "Hey there! It really is one hell of an uncomfortable night, isn't it? Do you take the bus here often?"

    charStranger "Me? The bus? No, I don't."

    charPlayer "Wow. What a coincidence that we are both trying to catch the bus home at the same time on the same day."

    charStranger "Yes, what a coincidence, huh? While we wait, fancy a drink? I still have a beer left. Do you want it?"

    charPlayer "Nah, I'm fine."

    charStranger "Oh, give it a try. It's really good. I promise."

    narrator "Aaah screw it. The night can't get any worse. Then at least you score a free beer."
    charPlayer "Yeah, come on. Pass on the bottle."

    charStranger "With the greatest pleasure. Here! Take it."

    narrator "The guy holds out a bottle to you. But it's so far away that you have to get up to grab it."
    charPlayer "Thanks for the beer. Nasty weather tonight, isn't it?"
    narrator "You take a strong sip from the bottle while the rain hits your face. And... actually it tastes really good."

    charStranger "Do you like it? I didn't promise too much, did I?"

    charPlayer "No, it's really g-... really g-"
    narrator "You suddenly feel dizzy. You're having a hard time keeping yourself on your feet."
    scene black with fade
    narrator "Then you stumble right in front of the strange guys. You can just barely catch yourself with your hands."

    charStranger "You should have told me that you can't stand something like that. Come on. Let me help you."

    narrator "Your eyes go black. All you can feel is someone dragging you away."

    stop music fadeout 1.0
    stop ambient fadeout 1.0
    hide rain onlayer ambientOverlay
    scene black with fade
    show screen endingScreen("Bad Ending 3/3\n\nYou missed the last bus home.\nYour body was never found.")
    pause
    
    # This ends the game.
    return



label tryingToLookBusy:

    narrator "You pick up your phone and start typing on it as if you had to write an important message to someone.."

    charStranger "Oh, just how can you stand this bright light. You'll ruin your beautiful eyes for sure. Trust me."

    charPlayer "Huh?"

    charStranger "You won't see well in the dark and you may miss something important."

    charPlayer "Wha... what do you want? Whatever it is, I'm not interested."
    narrator "Weirdly enough. Even though the stranger is standing so close to the bus stop, you can't really see more than his shadowy outline."
    narrator "Suddenly you remember that your phone has a selfie mode. This mode will just simply show a super bright white screen."
    narrator "This should help you see who this weird guy really is. Without further hesitation you point the cell phone screen towards the stranger's face."
    narrator "Instantly the stranger screams. It looks like his whole body is deforming. He's getting bigger and more misshapen."
    narrator "It's not human anymore. It's nothing like you've ever seen before."

    charStranger "Now you've really made me mad. You shouldn't have done that."

    narrator "It's trying to lunge at you. But something stops it. Some kind of force field."
    narrator "The bus stop light starts to flicker. The shadow creature's attacks now seem to be getting closer and closer to you."
    narrator "One of the giant bear-like claws almost caught you. You try to move away from the creature without taking your eyes off it, but you stumble."
    narrator "You find yourself lying on your back next to the damn trashcan."
    narrator "The lights of the bus stop are almost completely gone out and the beast makes a giant leap towards you."
    
    scene white with flash

    narrator "All of a sudden a blinding bright flash of light lights up the entire area."
    narrator "Then you hear tires screeching. A blood-curdling scream. And the bus you had been waiting for the whole time crashing into the bus stop."

    scene black with fade

    narrator "You can't believe it. What did just happen?"
    narrator "As if controlled by someone else, you pick up your cell phone and dial the emergency number."
    narrator "Wait!"
    narrator "Your cell phone has network! As you stare at your cell phone in disbelief, you hear a voice on the other end asking you what your emergency is."
    charPlayer "I... I think my bus just crashed into the bus stop."

    stop music fadeout 1.0
    stop ambient fadeout 1.0
    hide rain onlayer ambientOverlay
    scene black with fade
    show screen endingScreen("Good Ending 1/2\n\nQuite literally a bus stop.")
    pause

    # This ends the game.
    return



label ignoringTheStranger:

    charStranger "Oh. I guess you're not the talkative type, huh?"
    charStranger "That's fine."

    narrator "You just sat and waited. It was real strange."
    narrator "The stranger asked you a thousand things and always seemed to be waiting for an answer from you. He offered you drinks and food."
    narrator "You just sat and waited."
    narrator "The minutes felt like hours. You weren't sure what he really wanted from you. But also, you didn't really care."
    narrator "When the bus finally arrived, you entered the bus without even looking at the stranger."

    scene black with fade
    narrator "But when you sat down in the bus, your curiosity drove you to take one last look at the stranger."
    narrator "No one. There was no one there anymore."
    narrator "Did he get on the bus too? You looked around, but there was no one in sight except you and the bus driver."

    stop music fadeout 1.0
    stop ambient fadeout 1.0
    hide rain onlayer ambientOverlay
    scene black with fade
    show screen endingScreen("Good Ending 2/2\n\nJust a strange night.")


    # This ends the game.
    return

    
label ending_failSave:
    # This ends the game.
    stop music fadeout 1.0
    stop ambient fadeout 1.0
    hide rain onlayer ambientOverlay
    scene black with fade
    debugProgrammer "Who are you?"
    debugProgrammer "What are you doing here?"
    debugProgrammer "YoU aRe NoT sUpPoSeD tO Be HeRe!"
    debugProgrammer "{size=+20}G E T  O U T !{w=2.0}{nw}"
    return


###################################################################################################################################################
######################################################### OLD GAME STUFF ##########################################################################
###################################################################################################################################################